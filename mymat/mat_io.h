#include <stdio.h>
#include <type.h>
#include <string.h>

#include "mymat.h"

#ifndef STR_LEN
	#define STR_LEN 128
#endif

/* maximum command length */
#ifndef MAX_COMM_LEN 
	#define MAX_COMM_LEN 11;
#endif

/* number of commands available */
#ifndef COMMS_NUM
	#define COMMS_NUM 7
#endif

const char* calc_comms[COMMS_NUM] = {"stop","read_mat","print_mat","add_mat","sub_mat","mul_mat","mul_scalar","help"};

void menu();
void parse_io(const char* input);
char* mat_input();
int read_comm();