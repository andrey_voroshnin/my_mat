#include "mat.h"

float* add_mat(const float* mat_a, const float* mat_b)
{
	float* temp_mat = (float*)malloc(sizeof(float)*N*N);
	int i = 0;

	for (i = 0; i < N*N; i++)
	{
		temp_mat[i] = mat_a[i] + mat_b[i];
	}
	return temp_mat;
}

float* sub_mat(const float* mat_a, const float* mat_b)
{
	float* temp_mat = (float*)malloc(sizeof(float)*N*N);
	int i = 0;

	for (i = 0; i < N*N; i++)
	{
		temp_mat[i] = mat_a[i] - mat_b[i];
	}
	return temp_mat;
}

float* mul_scalar(const float* mat_a, const float val)
{
	float* temp_mat = (float*)malloc(sizeof(float)*N*N);
	int i = 0;

	for (i = 0; i < N*N; i++)
	{
		temp_mat[i] = mat_a[i] * val;
	}
	return temp_mat;
}

float* mul_mat(const float* mat_a, const float* mat_b)
{
	float* temp_mat = (float*)malloc(sizeof(float)*N*N);
	int i = 0;
	int j = 0;
	int r = 0;
	int c = 0;

	for (i = 0; i < N*N; i++)
	{
		for (r = 0; r < N; r++)
		{
			j = 0;
			for (c = 0; c < N*N; c += 4)
			{
				temp_mat[i] += mat_a[j+(int)r/N] * mat_b[c];
				j++;
			}
		}
	}

	return temp_mat;
}