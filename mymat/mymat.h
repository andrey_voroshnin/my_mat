#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "mat.h"

/* initial str length used for malloc */
#ifndef STR_LEN
	#define STR_LEN 128
#endif

/* maximum command length */
#ifndef MAX_COMM_LEN 
	#define MAX_COMM_LEN 11
#endif

/* number of commands available */
#ifndef COMMS_NUM
	#define COMMS_NUM 8
#endif

/* matrix size */
#ifndef N
	#define N 4
#endif

/* number of matrices */
#ifndef MAT_N
	#define MAT_N 6
#endif

/* matrix name length */
#ifndef MAT_NAME_LEN
	#define MAT_NAME_LEN 6
#endif

typedef struct matrix_def {
	char* name;
	float matrix[N*N];
} mat;