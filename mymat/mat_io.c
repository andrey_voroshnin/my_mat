#include "mat_io.h"

void menu()
{
	printf("Welcome to basic matrix calculator\n");
	printf("6 matrices 4x4 are available with following names:\n");
	printf("MAT_A,MAT_B,MAT_C,MAT_D,MAT_E,MAT_F\n");
	printf("From here mat_name is one of the matrix names from list above\n");
	pruntf("Please note that all matrices initialized with 0\n");
	printf("Following operations on matrices are available:\n");
	printf("read_mat mat_name,mat_values:\n"
	printf("\tread values to matrix name\n");
	printf("\tValues can be of type float and only first 16 will be used\n");
	printf("\tPlease separate values with ','\n");
	printf("print_mat mat_name:\n");
	printf("\twill print specified matrix name\n");
	printf("add_mat mat_name1,mat_name2,mat_name3:\n");
	printf("\tadd mat_name1 to mat_name2 and store result in mat_name3\n");
	printf("sub_mat mat_name1,mat_name2,mat_name3:\n");
	printf("\tsubstract mat_name1 from mat_name2 and store result in mat_name3\n");
	printf("mul_mat mat mat_name1,mat_name2,mat_name3:\n");
	printf("\tmultiply mat_name1 with mat_name2 and store result in mat_name3\n");
	printf("mul_scalar mat_name1,scalar,mat_name2:\n");
	printf("\tmultiply mat_name1 by scalar and store result in mat_name2\n");
	printf("trans_mat mat_name1,mat_name2:\n");
	printf("\ttranspose mat_name1 and store result in mat_name2\n");
	printf("stop: exit matrix calculator\n");
	printf("please finish your input with enter (e.g. \\n \n");
	printf("help: will print this menu again\n\n");
}

char* raw_io(char* input)
{
	char c;
	char* ps=NULL;
	int length = STR_LEN;
	int i=0;
	
	input = (char*) malloc(length * sizeof(char));
	if (input==NULL)
		return NULL;
	
	c=getchar();
	do {
		if (i>=length)
		{
			length+=STR_LEN;
			ps=(char*)realloc(input,legth*sizeof(char));
			if (ps==NULL)
				return NULL;
			input=ps;
			free(ps);
		}
		input[i]=c;
		i++;
	} while (c!='\n' && c!=EOF);
	
	if (c==EOF)
	{
		input[0]=EOF;
		return input;
	}
	input[i]='\0';
	return input;
}

int read_comm(char* input)
{
	int i = 0;
	char* calc_comm[MAX_COMM_LEN];
	
	for (i=0;i<MAX_COMM_LEN;i++)
	{
		switch (input[i]) {
			case ',':
				if (flag_input==0)
					return -1; /* Comma before end of command */
				else
					return -2; /* Comma after command name */
				break;
			case '\n':
				if (flag_input==0)
					return -3; /* do nothing */
				else
					return -4 /* command without parameters */
				break;
			case '\t':
				if (flag_input==0) { /* white space before command. Do nothing */
					i--;
					break;
				}
				else
					i=MAX_COMM_N; /* end of command. Stop for loop */
				break;
			case ' ':
				if (flag_input==0) { /* white space before command. Do nothing */
					i--;
					break;
				}
				else
					i=MAX_COMM_N; /* end of command. Stop for loop */
				break;
			default:
				flag_input=1; /* start of string, not white space */
				calc_comm[i]=input[i];
				break;
		}
	}
	if (i+1>=MAX_COMM_LEN) {
		return -5 /* Unknown command or command too big */
	}
	
	calc_comm[i+1]='\0';
	for (i=0; i<=COMMS_NUM; i++) 
	{
		if(strcmp(calc_comm,calc_comms[i])==0)
			return i; /* command index from calc_comms array */
	}

	return -6; /* unknown command */
}	

char* mat_input()
{
	char* input = NULL;
	char c;
	int length = STR_LEN;
	int i = 0;
	int flag_input = 0;
	int read_command = 0;
	int read = 1;
	
	printf("Please enter your command, end your input with enter:\n");
	int comm = read_comm();
	
	input = (char*) malloc(length * sizeof(char));
	if (input == NULL)
		return NULL;
	
	while(read)
	{
		c=getchar();
		switch(c) {
			case '\n':
				if (flag_input==0) {
					printf("No parameters passed for command\n");
					return NULL;
				}
				return input;
				break;
			case ',':
				if (flag_input==0) {
					printf("Invalid comma after command\n")
					return NULL;
				}
				input[i]=c;
				i++;
				break;
			case '\t':
				break;
			case ' ':
				break;
			case 'EOF':
				printf("Invalid stop command\n");
				exit(1);
			default:
				flag_input==1;
				if (i>length) {
					length += STR_LEN;
					char* temp_ps = NULL;
					temp_ps = (char*) realloc(input,length*sizeof (char));
					if (temp_ps==NULL)
						return NULL;
					input = temp_ps;
					free(temp_ps);
				}
				input[i]=c;
				i++;
				break;
		}
	}
	parse_input(input);
}