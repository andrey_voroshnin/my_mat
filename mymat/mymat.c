#include "mymat.h"
#pragma warning(disable : 4996)

void menu()
{
	printf("Welcome to basic matrix calculator\n");
	printf("6 matrices 4x4 are available with following names:\n");
	printf("MAT_A,MAT_B,MAT_C,MAT_D,MAT_E,MAT_F\n");
	printf("From here mat_name is one of the matrix names from list above\n");
	printf("Please note that all matrices initialized with 0\n");
	printf("Following operations on matrices are available:\n");
	printf("read_mat mat_name,mat_values:\n");
	printf("\tread values to matrix name\n");
	printf("\tValues can be of type float and only first 16 will be used\n");
	printf("\tPlease separate values with ','\n");
	printf("print_mat mat_name:\n");
	printf("\twill print specified matrix name\n");
	printf("add_mat mat_name1,mat_name2,mat_name3:\n");
	printf("\tadd mat_name1 to mat_name2 and store result in mat_name3\n");
	printf("sub_mat mat_name1,mat_name2,mat_name3:\n");
	printf("\tsubstract mat_name1 from mat_name2 and store result in mat_name3\n");
	printf("mul_mat mat mat_name1,mat_name2,mat_name3:\n");
	printf("\tmultiply mat_name1 with mat_name2 and store result in mat_name3\n");
	printf("mul_scalar mat_name1,scalar,mat_name2:\n");
	printf("\tmultiply mat_name1 by scalar and store result in mat_name2\n");
	printf("trans_mat mat_name1,mat_name2:\n");
	printf("\ttranspose mat_name1 and store result in mat_name2\n");
	printf("stop: exit matrix calculator\n");
	printf("please finish your input with enter (e.g. \\n)\n");
	printf("help: will print this menu again\n");
}

char* raw_io(char* input)
{
	char c;
	char* ps=NULL;
	int length = STR_LEN;
	int i=0;
	
	input = (char*) malloc(length * sizeof(char));
	if (input==NULL)
		return NULL;
	
	c=getchar();
	do {
		if (i>=length)
		{
			length+=STR_LEN;
			ps=(char*)realloc(input,length*sizeof(char));
			if (ps==NULL)
				return NULL;
			input=ps;
			free(ps);
		}
		input[i]=c;
		i++;
		c = getchar();
	} while (c!='\n' && c!=EOF);
	
	if (c==EOF)
	{
		input[0]=EOF;
		return input;
	}
	input[i]='\0';
	return input;
}

int read_comm(char* input, const char** calc_comms)
{
	int i = 0;
	int j = 0;
	char calc_comm[MAX_COMM_LEN+1];
	int flag_input = 0;
	int flag_stop = 0;
	
	while(!flag_stop)
	{
		switch (input[i]) {
			case ',':
				if (flag_input == 0) {
					printf("Comma before end of command\n");
					return -1; /* Comma before end of command */
				}
				else {
					printf("Comma after command name\n");
					return -1; /* Comma after command name */
				}
				break;
			case '\n':
				if (flag_input==0)
					return -1; /* do nothing */
				else {
					printf("Command without parameters\n");
					return -1; /* command without parameters */
				}
				break;
			case '\t':
				if (flag_input==0) { /* white space before command. Do nothing */
					i++;
					break;
				}
				calc_comm[j] = '\0';
				flag_stop = 1;
				break;
			case ' ':
				if (flag_input == 0) { /* white space before command. Do nothing */
					i++;
					break;
				}
				calc_comm[j] = '\0';
				flag_stop = 1; /* end of command. Stop for loop */
				break;
			case '\0':
				/* printf("Command without parameters\n"); */
				flag_stop = 1;
				break;
			default:
				flag_input = 1; /* start of string, not white space */
				if (j >= MAX_COMM_LEN)
				{
					printf("Space missing or unknown command\n");
					return -1;
				}
				calc_comm[j] = input[i];
				j++;
				i++;
				break;
		}
	}

	calc_comm[MAX_COMM_LEN] = '\0';
	if (j > MAX_COMM_LEN) {
		printf("Unknown comand");
		return -1; /* Unknown command or command too big */
	}
	j = i;	
	for (i = 0; i < COMMS_NUM; i++)
	{
		if (strcmp(calc_comm, calc_comms[i]) == 0)
		{
			input = strcpy(input, input + j);
			return i; /* command index from calc_comms array */
		}
	}

	printf("Unknown command\n");
	return -1; /* unknown command */
}

int find_mat(const char* name, mat* matrices)
{
	int i = 0;

	for (i = 0; i < MAT_N; i++)
	{
		if (!(strcmp(matrices[i].name, name)))
			return i;
	}

	return -1; /* mat not found */
}

int read_mat_name(char* input, mat* matrices)
{
	int i = 0;
	int j = 0;
	char mat_name[MAT_NAME_LEN];
	int mat_index = -1;
	int flag_start = 0;

	while (input[i] != '\0' && input[i] != ',')
	{
		if (isalpha(input[i]) || input[i] == '_')
		{
			flag_start = 1;
			mat_name[j] = input[i];
			i++;
			j++;
		}
		else if (isspace(input[i])) {
			i++;
			continue;
		}
		else if (flag_start == 1) {
			printf("Invalid characters in matrix name\n");
			return -1;
		}
	}

	if (j != MAT_NAME_LEN - 1) {
		printf("Invalid matrix name.\n");
		return -1;
	}

	mat_name[j] = '\0';
	mat_index = find_mat(mat_name, matrices);

	if (mat_index == -1) {
		printf("Undefined matrix name\n");
		return -2;
	}

	i++;
	input = strcpy(input, input + i);
	return mat_index;
}

int parse_read_mat(char* input, mat* matrices)
{
	int i = 0;
	int j = 0;
	int mat_index = -1;
	char ps[STR_LEN];
	float val;
	int cnt = -1;

	if (mat_index = read_mat_name(input, matrices) == -1)
		return -1;
	if (mat_index < 0)
		return -1;

	while (input[i] != '\0')
	{
		while (input[i] != '\0' && input[i] != ',')
		{
			if (isspace(input[i])) {
				i++;
				continue;
			}
			else if (isalnum(input[i]) || input[i] == '.') {
				ps[j] = input[i];
				j++;
				i++;
			}
		}
		cnt++;
		ps[j] = '\0';
		j = 0;
		val = (float)atof(ps);
		if (val != 0) {
			matrices[mat_index].matrix[cnt] = val;
		}
		else {
			printf("Bad number entered\n");
			return -1;
		}
		ps[0] = '\0';
		if (input[i] == ',')
			i++;
	}
	return 0;
}

void print_mat(const char* mat_name, float* matrix)
{
	int i=0;
	int j=0;
	
	printf("Printing matrix %s:\n",mat_name);
	
	for (i = 0; i < N*N; i += N)
	{
		for (j = 0; j < N; j++)
		{
			printf("%.2f\t", matrix[i+j]);
		}
		printf("\n");
	}
}

void parse_print_mat(char* input, mat* matrices)
{
	int mat_index = read_mat_name(input, matrices);
	if (mat_index>=0)
		print_mat(matrices[mat_index].name, matrices[mat_index].matrix);
	return;
}

void parse_add_mat(char* input, mat* matrices)
{
	int i = 0;
	int mat_i1, mat_i2, mat_i3;
	float* temp_mat;
	char* ps;

	ps = strtok(input, ",");

	if (mat_i1 = read_mat_name(ps, matrices) == -1)
		return;
	ps = strtok(NULL, ",");
	if (mat_i2 = read_mat_name(ps, matrices) == -1)
		return;
	ps = strtok(NULL, ",");
	if (mat_i3 = read_mat_name(ps, matrices) == -1)
		return;

	temp_mat = add_mat(matrices[mat_i1].matrix, matrices[mat_i2].matrix);
	for (i = 0; i < N*N; i++)
		matrices[mat_i3].matrix[i] = temp_mat[i];
	free(temp_mat);
	return;
}

void parse_sub_mat(char* input, mat* matrices)
{
	int i = 0;
	int mat_i1, mat_i2, mat_i3;
	float* temp_mat;
	char* ps;

	ps = strtok(input, ",");

	if (mat_i1 = read_mat_name(ps, matrices) == -1)
		return;
	ps = strtok(NULL, ",");
	if (mat_i2 = read_mat_name(ps, matrices) == -1)
		return;
	ps = strtok(NULL, ",");
	if (mat_i3 = read_mat_name(ps, matrices) == -1)
		return;

	temp_mat = sub_mat(matrices[mat_i1].matrix, matrices[mat_i2].matrix);
	for (i = 0; i < N*N; i++)
		matrices[mat_i3].matrix[i] = temp_mat[i];
	free(temp_mat);
	return;
}

void parse_mul_mat(char* input, mat* matrices)
{
	int i = 0;
	int mat_i1, mat_i2, mat_i3;
	float* temp_mat;
	char* ps;

	ps = strtok(input, ",");

	if (mat_i1 = read_mat_name(ps, matrices) == -1)
		return;
	ps = strtok(NULL, ",");
	if (mat_i2 = read_mat_name(ps, matrices) == -1)
		return;
	ps = strtok(NULL, ",");
	if (mat_i3 = read_mat_name(ps, matrices) == -1)
		return;

	temp_mat = mul_mat(matrices[mat_i1].matrix, matrices[mat_i2].matrix);
	for (i = 0; i < N*N; i++)
		matrices[mat_i3].matrix[i] = temp_mat[i];
	free(temp_mat);
	return;
}

void parse_mul_scalar(char* input, mat* matrices)
{
	int i = 0;
	int mat_i1;
	float* temp_mat;
	char* ps;
	float val;

	ps = strtok(input, ",");

	if (mat_i1 = read_mat_name(ps, matrices) == -1)
		return;
	ps = strtok(NULL, ",");
	val = (float)atof(ps);

	temp_mat = mul_scalar(matrices[mat_i1].matrix, val);
	for (i = 0; i < N*N; i++)
		matrices[mat_i1].matrix[i] = temp_mat[i];
	free(temp_mat);
	return;
}

int parse_trans_mat(char* input, mat* matrices);

int read_mat(const char* mat_name, float* matrix);

int main()
{
	char* raw_input = NULL;
	int comm = 0;
	const char* calc_comms[COMMS_NUM] = {"stop","read_mat","print_mat","add_mat","sub_mat","mul_mat","mul_scalar","help"};
	mat matrices[MAT_N] = {
		{ "MAT_A", {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} },
		{ "MAT_B", {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} },
		{ "MAT_C", {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} },
		{ "MAT_D", {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} },
		{ "MAT_E", {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} },
		{ "MAT_F", {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} } };
	
	menu();
	printf("Please enter your command:\n");
	raw_input = raw_io(raw_input);
	if(raw_input[0]==EOF) {
		printf("Wrong exit command. Please use 'stop'\n");
		return 1;
	}
	comm = read_comm(raw_input,calc_comms);
	while (comm!=0)
	{
		switch(comm) {
			case -1: /*General error break*/
				break;
			case 0: /* Stop command */
				break;
			case 1: /* read_mat */
				parse_read_mat(raw_input,matrices);
				break;
			case 2: /* print_mat */
				parse_print_mat(raw_input,matrices);
				break;
			case 3: /*add_mat */
				parse_add_mat(raw_input, matrices);
				break;
			case 4: /* sub_mat */
				parse_sub_mat(raw_input, matrices);
				break;
			case 5: /* mul_mat */
				parse_mul_mat(raw_input, matrices);
				break;
			case 6: /* mul_scalar */
				parse_mul_scalar(raw_input, matrices);
				break;
			case 7: /* help */
				menu();
				break;
			default: /* default should be available only if blank enter pressed */
				printf("Available commands are 'stop,read_mat,print_mat,add_mat,sub_mat,mul_mat,mul_scalar,help':\n");
				break;
		}
		printf("Please enter your command:\n");
		free(raw_input); /* need to free raw input string before */
		raw_input = raw_io(raw_input);
		comm = read_comm(raw_input,calc_comms);
	}
	
	printf("Thank you and goodbye\n");
	return 0;
}