#include <stdlib.h>
#ifndef N
	#define N 4
#endif

float* add_mat(const float* mat_a, const float* mat_b);
float* sub_mat(const float* mat_a, const float* mat_b);
float* mul_mat(const float* mat_a, const float* mat_b);
float* mul_scalar(const float* mat_a, const float val);
float* trans_mat(const float* mat_a);